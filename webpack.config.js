const CopyPlugin = require('copy-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const HtmlWebPackPlugin = require("html-webpack-plugin")
const webpack = require('webpack')
var path = require('path')

module.exports = {
    devServer:{ //what the localhost?
        contentBase: path.resolve(__dirname,'dist'),
        port: 3000
    },
    entry: path.resolve(__dirname, 'src/index.jsx'),
    output: {
        /*Webpack producing results*/
        path: path.resolve(__dirname, "dist"),
        filename: "index.js"
    },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(ts|tsx)$/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },{
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },{
        test: /\.(jpe?g|png|svg)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })]
}